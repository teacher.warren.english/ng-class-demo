import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { JokesPageComponent } from "./pages/jokes-page/jokes-page.component";
import { LoginPageComponent } from "./pages/login-page/login-page.component";

const routes:Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
    },
    {
        path: 'login',
        component: LoginPageComponent
    },
    {
        path: 'home',
        component: HomePageComponent
    },
    {
        path: 'jokes',
        component: JokesPageComponent,
        canActivate: [AuthGuard]
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}