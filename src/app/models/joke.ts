export type Joke = {
    id: number,
    setup: string,
    delivery: string,
}