export interface Flags {
    nsfw: boolean;
    religious: boolean;
    racist: boolean;
    sexist: boolean;
    political: boolean;
    explicit: boolean;
}

export interface JokeResponse {
    error: boolean;
    category: string;
    type: string;
    setup: string;
    delivery: string;
    flags: Flags;
    id: number;
    safe: boolean;
    lang: string;
}