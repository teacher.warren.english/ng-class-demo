import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { map, Observable } from 'rxjs';
import { User } from '../models/user';
import { UserResponse } from '../models/userResponse';

@Injectable({
    providedIn: 'root'
})
export default class LoginService {
    constructor(private readonly http: HttpClient) { }

    private _currentUser: User = {
        id: '',
        firstname: '',
        lastname: '',
        email: '',
        profilePicUrl: ''
    }
    
    public get currentUser() : User {
        return this._currentUser
    }    

    loginAttempt(): void {
        this.http.get<UserResponse>('https://randomuser.me/api')
        .pipe(
            // chain RxJS Operators
            map((response) => {
              console.log(response.results[0])
              let parsedUser: User = {
                id: response.results[0].id.value,
                firstname: response.results[0].name.first,
                lastname: response.results[0].name.last,
                email: response.results[0].email,
                profilePicUrl: response.results[0].picture.medium
              }
              console.log(parsedUser)
              return parsedUser
            })
          )
          .subscribe({
            next: (user) => {
              this._currentUser = user
            },
            error: (error) => { console.error(error.message) }
          })
    }
}