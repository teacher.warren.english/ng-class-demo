import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Joke } from '../models/joke';
import { JokeResponse } from '../models/jokeResponse';

@Injectable({
  providedIn: 'root'
})
export class JokeService {

  constructor(private readonly http: HttpClient) { }

  private _nextJoke: Joke = {
    id: 0,
    setup: '',
    delivery: ''
  }

  public get newJoke(): Joke {
    return this._nextJoke
  }

  getJoke(): void {
    this.http.get<JokeResponse>('https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit')
      .pipe(
        map((response) => {
          console.log(response);
          
          let parsedJoke: Joke = {
            id: response.id,
            setup: response.setup,
            delivery: response.delivery
          }
          console.log(parsedJoke);
          
          return parsedJoke
        })
      )
      .subscribe({
        next: (parsedJoke) => {
          this._nextJoke = parsedJoke
        },
        error: (error) => {
          console.error(error.message);
        }
      })
  }
}
