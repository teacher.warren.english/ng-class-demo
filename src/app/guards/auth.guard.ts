import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import keycloak from "src/keycloak";
import LoginService from "../services/login.service";


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate{
    constructor(private readonly loginService: LoginService, private readonly router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        
        // with login service
        // this.loginService.currentUser
        if (keycloak.authenticated) {
            return true
        }

        this.router.navigateByUrl('login')
        return false
    }
}