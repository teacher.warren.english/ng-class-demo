import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor() { }

  token: string | undefined = ''

  ngOnInit(): void {
  }

  handleLoginWithKeycloak(): void {
    keycloak.login()
  }

  handleLogoutWithKeycloak(): void {
    keycloak.logout()
  }

  handleShowToken(): void {
    // show the token
    console.log("TOKEN", keycloak.token);
    console.log("PARSED TOKEN", keycloak.tokenParsed);
    this.token = keycloak.token?.toString()
  }

}
