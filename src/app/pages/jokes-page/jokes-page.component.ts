import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-jokes-page',
  templateUrl: './jokes-page.component.html',
  styleUrls: ['./jokes-page.component.css']
})
export class JokesPageComponent implements OnInit {

  constructor() { }

  user: User = {
    id: '',
    firstname: '',
    lastname: '',
    email: '',
    profilePicUrl: ''
  }

  ngOnInit(): void {

    this.user.email = keycloak.tokenParsed?.email ?? ''

    if (!keycloak.tokenParsed?.given_name || !keycloak.tokenParsed?.family_name || !keycloak.tokenParsed?.preferred_username || !keycloak.tokenParsed?.email){
      // token is undefined
      console.error("Token is undefined");
    } else
    {
      this.user.firstname = keycloak.tokenParsed?.given_name
      this.user.lastname = keycloak.tokenParsed?.family_name
      this.user.id = keycloak.tokenParsed?.preferred_username
      // this.user.email = keycloak.tokenParsed?.email
    }

  }

}
