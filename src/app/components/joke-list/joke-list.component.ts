import { Component, OnInit } from '@angular/core';
import { Joke } from 'src/app/models/joke';
import { JokeService } from 'src/app/services/joke.service';

@Component({
  selector: 'app-joke-list',
  templateUrl: './joke-list.component.html',
  styleUrls: ['./joke-list.component.css']
})
export class JokeListComponent implements OnInit {

  constructor(private readonly jokeService: JokeService) { }

  // jokeData: Joke = {
  //   id: 0,
  //   setup: '',
  //   delivery: ''
  // }

  public get jokeData(): Joke {
    return this.jokeService.newJoke
  }

  ngOnInit(): void {
    this.handleNextJoke()
    // this.jokeData.setup = 'How do you organize a space party?'
    // setTimeout(() => {
    //   this.jokeData.delivery = 'You planet.'
    // }, 2000);
  }

  handleNextJoke(): void {
    this.jokeService.getJoke()
    // this.jokeData.setup = "What's orange and sounds like a parrot?"
    // setTimeout(() => {
    //   this.jokeData.delivery = 'A carrot.'
    // }, 2000);
  }

  handleEmitReceived(): void {
    console.log("I have received your emission!");
    
  }
}
