import { Component } from '@angular/core';

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    constructor() { }

    // properties
    message: string = "Hello from the login component! 🙋‍♂️"
    bananaUrl: string = "https://pamsdailydish.com/wp-content/uploads/2015/04/Bunch-Bananas-1.jpg"

    isLoggedIn: boolean = false

    dailyTodos: string[] = ['eat', 'pray', 'love']
    // event handlers 

    // etc
}