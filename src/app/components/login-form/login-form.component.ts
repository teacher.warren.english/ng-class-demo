import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserResponse } from 'src/app/models/userResponse';
import LoginService from 'src/app/services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private readonly loginService: LoginService) { }

  
  public get currentUser() : User {
    return this.loginService.currentUser
  }

  ngOnInit(): void {
  }

  handleLogin(): void {
      this.loginService.loginAttempt()
      
  }

}
