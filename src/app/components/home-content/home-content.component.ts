import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user'

@Component({
  selector: 'app-home-content',
  templateUrl: './home-content.component.html',
  styleUrls: ['./home-content.component.css']
})
export class HomeContentComponent implements OnInit {

  constructor() {}

  email = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.email
  ])

  newUser: User = {
    id: '',
    firstname: '',
    lastname: '',
    email: '',
    profilePicUrl: ''
  }

  ngOnInit(): void {
  }

  handleOnSubmit(myNgForm : NgForm) { // Ctrl + .
    console.log(myNgForm);
    
    const { value } = myNgForm

    this.newUser.firstname = value.firstname
    this.newUser.lastname = value.lastname
    this.newUser.email = value.email

    console.log(this.newUser)
  }
}
