import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Joke } from 'src/app/models/joke';

@Component({
  selector: 'app-joke-list-item',
  templateUrl: './joke-list-item.component.html',
  styleUrls: ['./joke-list-item.component.css']
})
export class JokeListItemComponent implements OnInit {

  constructor() { }

  @Input() currentJoke:Joke = {
    id: 0,
    setup: '',
    delivery: ''
  }

  @Output() myOutput:EventEmitter<any> = new EventEmitter()

  handleEmitClick(): void {
    console.log("Event emitter");
    this.myOutput.emit()
  }

  ngOnInit(): void {
    
  }

}
