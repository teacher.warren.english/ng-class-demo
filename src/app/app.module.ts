import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login-component-lesson-1/login.component';
import { JokeListItemComponent } from './components/joke-list-item/joke-list-item.component';
import { JokeListComponent } from './components/joke-list/joke-list.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { HomeContentComponent } from './components/home-content/home-content.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { JokesPageComponent } from './pages/jokes-page/jokes-page.component';
import { AppRoutingModule } from './app.router.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    JokeListItemComponent,
    JokeListComponent,
    LoginFormComponent,
    HomeContentComponent,
    LoginPageComponent,
    HomePageComponent,
    JokesPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
